package cz.cvut.fit.ddw.stanej14.ddw_01.mvp.presenter;

import android.os.Bundle;

import cz.cvut.fit.ddw.stanej14.ddw_01.App;
import cz.cvut.fit.ddw.stanej14.ddw_01.mvp.view.ITwitterView;
import nucleus.presenter.RxPresenter;

/**
 * presenter for twitter fragment
 */
public class TwitterPresenter extends RxPresenter<ITwitterView> {
    public static final String TAG = TwitterPresenter.class.getName();

    @Override
    public void onCreate(final Bundle arguments) {
        super.onCreate(arguments);
        App.getAppComponent().inject(this);
    }

    @Override
    protected void onTakeView(ITwitterView iTwitterView) {
        super.onTakeView(iTwitterView);
        iTwitterView.setData("android");
    }
}

