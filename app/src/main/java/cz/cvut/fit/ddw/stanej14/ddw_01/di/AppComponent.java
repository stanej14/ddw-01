package cz.cvut.fit.ddw.stanej14.ddw_01.di;

import javax.inject.Singleton;

import cz.cvut.fit.ddw.stanej14.ddw_01.mvp.presenter.TwitterPresenter;
import dagger.Component;

/**
 * Main application component
 **/
@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {
    void inject(TwitterPresenter twitterPresenter);
}
