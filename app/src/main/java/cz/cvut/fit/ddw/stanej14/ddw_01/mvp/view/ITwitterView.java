package cz.cvut.fit.ddw.stanej14.ddw_01.mvp.view;

/**
 * Interface for the twitter view
 */
public interface ITwitterView {

    void setData(String tag);
}
