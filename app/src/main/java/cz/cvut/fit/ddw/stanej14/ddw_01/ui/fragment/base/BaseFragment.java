/**
 * BaseFragment.java
 *
 * @project Ulek
 * @package cz.eman.ulek.fragment.base.BaseFragment
 * @author eMan s.r.o.
 * @since 19.11.13 14:13
 */

package cz.cvut.fit.ddw.stanej14.ddw_01.ui.fragment.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import butterknife.ButterKnife;


public abstract class BaseFragment extends Fragment implements IBaseFragment {
    public static String TAG = BaseFragment.class.getName();

    private FragmentDelegate delegate;

    public BaseFragment() {
        delegate = new FragmentDelegate(this);
        if (getArguments() == null) {
            setArguments(new Bundle());
        }
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        delegate.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null) {
            setTitle(getTitle());
        }
    }


    protected void setTitle(int title) {
        delegate.setTitle(title);
    }

    protected void setTitle(String title) {
        delegate.setTitle(title);
    }

    protected void setSubtitle(String subTitle) {
        delegate.setSubtitle(subTitle);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        delegate.onPrepareOptionsMenu(menu);
        super.onPrepareOptionsMenu(menu);
    }

    protected abstract String getTitle();

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        delegate.onActivityCreated(savedInstanceState);
        initAB();
    }

    protected abstract void initAB();

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        hideIme();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        delegate.onDestroy();
    }

    /**
     * Hides keyboard if it is shown
     */
    protected void hideIme() {
    }

    @Override
    public void onConnectionRepeat() {

    }

    @Override
    public void onConnectionDialogCancel() {
    }

    /**
     * Called when back button is pressed
     */
    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onUpButtonClicked();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Called when up is clicked
     */
    public void onUpButtonClicked() {
    }

    @Override
    public void onStart() {
        super.onStart();
        delegate.onStart();
    }
}
