package cz.cvut.fit.ddw.stanej14.ddw_01.di;

import android.app.Application;

import javax.inject.Singleton;

import cz.cvut.fit.ddw.stanej14.ddw_01.App;
import dagger.Module;
import dagger.Provides;

/**
 * Module that handle injecting application class (ie Context)
 */
@Module
public class AppModule {
    public static final String TAG = AppModule.class.getName();
    private final App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Provides
    @Singleton
    public Application provideApplication() {
        return app;
    }
}
