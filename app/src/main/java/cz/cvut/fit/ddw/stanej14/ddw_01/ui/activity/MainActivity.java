package cz.cvut.fit.ddw.stanej14.ddw_01.ui.activity;

import cz.cvut.fit.ddw.stanej14.ddw_01.ui.activity.base.BaseFragmentActivity;
import cz.cvut.fit.ddw.stanej14.ddw_01.ui.fragment.TwitterFragment;

/**
 * Main activity
 */
public class MainActivity extends BaseFragmentActivity {

    @Override
    protected String getFragmentName() {
        return TwitterFragment.class.getName();
    }
}
