/**
 * IBaseFragment.java
 *
 * @project Ulek
 * @package cz.eman.ulek.fragment.base.IBaseFragment
 * @author eMan s.r.o.
 * @since 19.11.13 14:13
 */

package cz.cvut.fit.ddw.stanej14.ddw_01.ui.fragment.base;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

public interface IBaseFragment {
    FragmentManager getFragmentManager();

    FragmentActivity getActivity();

    /**
     * Method called from error dialog when user clicks on try again button
     */
    void onConnectionRepeat();

    /**
     * Method called from error dialog when user clicks on onConnectionDialogCancel button
     */
    void onConnectionDialogCancel();

    /**
     * Called when back button is pressed
     */
    boolean onBackPressed();

    /**
     * Get fragment arguments
     * @return
     */
    Bundle getArguments();

    /**
     * Called when UP button is clicked
     */
    void onUpButtonClicked();
}
