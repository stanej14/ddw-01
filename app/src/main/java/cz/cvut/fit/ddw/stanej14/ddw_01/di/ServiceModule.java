package cz.cvut.fit.ddw.stanej14.ddw_01.di;

import dagger.Module;

/**
 * Module providing api services
 **/
@Module
public class ServiceModule {
    public static final String TAG = ServiceModule.class.getName();

}
