package cz.cvut.fit.ddw.stanej14.ddw_01.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.tweetui.TimelineResult;
import com.twitter.sdk.android.tweetui.TweetTimelineListAdapter;
import com.twitter.sdk.android.tweetui.UserTimeline;

import butterknife.Bind;
import cz.cvut.fit.ddw.stanej14.ddw_01.R;
import cz.cvut.fit.ddw.stanej14.ddw_01.mvp.presenter.TwitterPresenter;
import cz.cvut.fit.ddw.stanej14.ddw_01.mvp.view.ITwitterView;
import cz.cvut.fit.ddw.stanej14.ddw_01.ui.fragment.base.BaseNucleusFragment;
import nucleus.factory.RequiresPresenter;

/**
 * Fragment with twitter statuses
 */
@RequiresPresenter(TwitterPresenter.class)
public class TwitterFragment extends BaseNucleusFragment<TwitterPresenter> implements ITwitterView {
    public static final String TAG = TwitterFragment.class.getName();

    public static final long JAKE_WHARTON_ID = 15221262;

    @Bind(R.id.layout_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @Bind(android.R.id.list)
    ListView listView;
    @Bind(android.R.id.progress)
    View progress;

    private TweetTimelineListAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_twitter, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void setData(String tag) {
        UserTimeline timeline = new UserTimeline.Builder()
                .userId(JAKE_WHARTON_ID)
                .build();


        adapter = new TweetTimelineListAdapter.Builder(getActivity()).setTimeline(timeline)
                .setOnActionCallback(new Callback<Tweet>() {
                    @Override
                    public void success(Result<Tweet> result) {
                        progress.setVisibility(View.GONE);

                        // Filter not relevant tweets
//                        try {
//                            ProcessingResource documentResetPR = (ProcessingResource) Factory.createResource("gate.creole.annotdelete.AnnotationDeletePR");
//                            ProcessingResource sentenceSplitterPR = (ProcessingResource) Factory.createResource("gate.creole.splitter.SentenceSplitter");
//                            ProcessingResource tokenizerPR = (ProcessingResource) Factory.createResource("gate.creole.tokeniser.DefaultTokeniser");
//                            SerialAnalyserController pipeline = (SerialAnalyserController) Factory.createResource("gate.creole.SerialAnalyserController");
//
//                            pipeline.add(documentResetPR);
//                            pipeline.add(tokenizerPR);
//                            pipeline.add(sentenceSplitterPR);
//
//                            // create a document
//                            Document document = Factory.newDocument("This is some initial text");
//                            // create a corpus and add the document
//                            Corpus corpus = Factory.newCorpus("");
//                            corpus.add(document);
//
//                            pipeline.setCorpus(corpus);
//                            pipeline.execute();
//                        } catch (ResourceInstantiationException e) {
//                            e.printStackTrace();
//                        } catch (ExecutionException e) {
//                            e.printStackTrace();
//                        }

                    }

                    @Override
                    public void failure(TwitterException e) {

                    }
                })
                .build();

        listView.setAdapter(adapter);
        swipeRefreshLayout.setOnRefreshListener(() -> adapter.refresh(new Callback<TimelineResult<Tweet>>() {
            @Override
            public void success(Result<TimelineResult<Tweet>> result) {
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void failure(TwitterException e) {
                swipeRefreshLayout.setRefreshing(false);
            }
        }));
    }

    @Override
    protected String getTitle() {
        return null;
    }

    @Override
    protected void initAB() {
    }


}
