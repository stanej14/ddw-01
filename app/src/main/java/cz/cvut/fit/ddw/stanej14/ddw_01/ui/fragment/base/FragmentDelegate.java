package cz.cvut.fit.ddw.stanej14.ddw_01.ui.fragment.base;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import cz.cvut.fit.ddw.stanej14.ddw_01.ui.activity.base.BaseFragmentActivity;

/**
 * Common class for all types of fragments
 */
public class FragmentDelegate {
    static final String TAG = "FragmentDelegate";
    private static final int MENU_ID_REFRESH = 1;
    private final IBaseFragment fragment;
    private boolean progressBarVisible;
    private boolean refreshVisible;

    public FragmentDelegate(IBaseFragment fragment) {
        this.fragment = fragment;
    }

    private BaseFragmentActivity getActivity() {
        return (BaseFragmentActivity) fragment.getActivity();
    }


    void setTitle(int title) {
        setTitle(getActivity().getString(title));
        setSubtitle(null);
    }

    void setTitle(String title) {
        if (TextUtils.isEmpty(title)) {
            return;
        }
        getActivity().setTitle(title);
    }

    void setSubtitle(String subTitle) {
        getActivity().setSubtitle(subTitle);
    }

    void onCreate(Bundle savedInstanceState) {

    }

    void onActivityCreated(Bundle savedInstanceState) {
    }


    void showDialog(final DialogFragment dialog, final String tag) {
        new Handler().post(new Runnable() {

            @Override
            public void run() {
                dialog.show(fragment.getFragmentManager(), tag);
            }
        });

    }

    void dismissDialog(final String tag) {
        new Handler().post(new Runnable() {

            @Override
            public void run() {
                DialogFragment dialog = (DialogFragment) fragment.getFragmentManager().findFragmentByTag(tag);
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    void setProgressShown(boolean shown) {
        Log.d(TAG, "setProgressShown" + shown);
        progressBarVisible = shown;
        getActivity().setSupportProgressBarIndeterminateVisibility(shown);

        getActivity().supportInvalidateOptionsMenu();
    }

    void onPrepareOptionsMenu(Menu menu) {
        Log.d(TAG, "onPrepareOptionsMenu");
        MenuItem item = menu.findItem(MENU_ID_REFRESH);
        if (item != null) {
            Log.d(TAG, "item not null, visibility=" + progressBarVisible);
            item.setVisible(!progressBarVisible && refreshVisible);
        }
    }

    void finish() {
        if (fragment.getFragmentManager().getBackStackEntryCount() > 0) {
            fragment.getFragmentManager().popBackStack();
        } else {
            getActivity().finish();
        }
    }

    void showRefresh(boolean show) {
        refreshVisible = show;
        getActivity().supportInvalidateOptionsMenu();
    }


    /**
     * Basic settings of actionbar
     */
    protected void baseSettingsAB() {
        ActionBar ab = getActivity().getSupportActionBar();
        ab.setDisplayShowTitleEnabled(true);
        ab.setDisplayShowHomeEnabled(true);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    public void onStart() {
    }

    public void onDestroy() {
    }
}
