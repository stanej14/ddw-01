package cz.cvut.fit.ddw.stanej14.ddw_01.ui.activity.base;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import cz.cvut.fit.ddw.stanej14.ddw_01.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class BaseActivity extends AppCompatActivity {
    public static String TAG = BaseActivity.class.getName();

    public void setSubtitle(int resId) {
        setSubtitle(getString(resId));
    }

    public void setSubtitle(String text) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setSubtitle(text);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkSkeletonThings();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    /**
     * Checks if developer correctly sets this projects properties
     */
    private void checkSkeletonThings() {
        if (getString(R.string.app_name).contains("skeleton")) {
            throw new IllegalStateException("Name your application with other name in strings.xml");
        }
        ActivityInfo activityInfo;
        try {
            activityInfo = getPackageManager().getActivityInfo(getComponentName(), PackageManager.GET_META_DATA);
            String title = activityInfo.loadLabel(getPackageManager()).toString();
            if (title.contains("Skeleton")) {
                throw new IllegalStateException("Name your application with other name in gradle.properties");
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }
}
