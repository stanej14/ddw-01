package cz.cvut.fit.ddw.stanej14.ddw_01;

import android.app.Application;
import android.support.annotation.NonNull;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import cz.cvut.fit.ddw.stanej14.ddw_01.di.AppComponent;
import cz.cvut.fit.ddw.stanej14.ddw_01.di.AppModule;
import cz.cvut.fit.ddw.stanej14.ddw_01.di.DaggerAppComponent;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


/**
 * Application class for this application
 */
public class App extends Application {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "UijHG8KmuFvGJUUazXRPEfNCd";
    private static final String TWITTER_SECRET = "tVVfkD2PLHl7bELVoDVeS5XWhwD3EJyuqyCXlAVC3gkHjerD1T";


    public static final String PACKAGE_NAME = BuildConfig.APPLICATION_ID;
    private static AppComponent appComponent;

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    public static void setAppComponent(@NonNull AppComponent component) {
        appComponent = component;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Init Gate
//        try {
//            Gate.init();
//        } catch (GateException e) {
//            e.printStackTrace();
//        }

        // Create dagger object graph
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();

        // Caligraphy
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(getString(R.string.roboto))
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        // Twitter
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
    }
}
